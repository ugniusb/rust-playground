use async_std::sync::RwLock;
use serde::Deserialize;
use serde::Serialize;
use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::sync::Arc;
use tide::Body;
use tide::Request;
use tide::Response;
use tide::Server;

#[derive(Debug, Deserialize, Serialize, Clone, PartialEq, Eq, Ord, PartialOrd)]
struct Dino {
    name: String,
    weight: i32,
    diet: String,
}

type Ref<D> = Arc<RwLock<D>>;

#[derive(Clone, Debug)]
struct State {
    dinos: Ref<HashMap<String, Dino>>,
}

#[async_std::main]
async fn main() {
    tide::log::start();
    let store = Default::default();
    let app = server(store).await;
    app.listen("127.0.0.1:8080").await.unwrap();
}

async fn server(store: Ref<HashMap<String, Dino>>) -> Server<State> {
    let state = State { dinos: store };
    let mut app = tide::with_state(state);
    app.at("/").get(|_| async { Ok("Hello, world!") });
    app.at("/dinos")
        .post(|mut req: Request<State>| async move {
            let dino: Dino = req.body_json().await?;
            let mut dinos = req.state().dinos.write().await;
            dinos.insert(String::from(&dino.name), dino.clone());
            println!("{:?}", dino);
            let mut res = Response::new(201);
            res.set_body(Body::from_json(&dino)?);
            Ok(res)
        })
        .get(|req: Request<State>| async move {
            let dinos = req.state().dinos.read().await;
            let dinos_vec = dinos.values().cloned().collect::<Vec<Dino>>();
            let mut res = Response::new(200);
            res.set_body(Body::from_json(&dinos_vec)?);
            Ok(res)
        });
    app.at("/dinos/:name")
        .get(|req: Request<State>| async move {
            let dinos = req.state().dinos.read().await;
            let key = req.param("name")?;
            let res = match dinos.get(key) {
                None => Response::new(404),
                Some(x) => {
                    let mut res = Response::new(200);
                    res.set_body(Body::from_json(x)?);
                    res
                }
            };
            Ok(res)
        })
        .put(|mut req: Request<State>| async move {
            let dino_update = req.body_json::<Dino>().await?;
            let mut dinos = req.state().dinos.write().await;
            let res = match dinos.entry(req.param("name")?.to_string()) {
                Entry::Vacant(_) => Response::new(404),
                Entry::Occupied(mut x) => {
                    *x.get_mut() = dino_update;
                    let mut res = Response::new(200);
                    res.set_body(Body::from_json(x.get())?);
                    res
                }
            };
            Ok(res)
        })
        .delete(|req: Request<State>| async move {
            let mut dinos = req.state().dinos.write().await;
            let deleted = dinos.remove(req.param("name")?);
            Ok(Response::new(match deleted {
                None => 404,
                Some(_) => 200,
            }))
        });
    app
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde::de::DeserializeOwned;
    use tide::http::{Method, Request, Response, Url};

    #[derive(Debug)]
    struct Resp<T: DeserializeOwned> {
        raw: Response,
        parsed: tide::Result<T>,
    }

    fn mk_dino(name: &str) -> Dino {
        Dino {
            name: String::from(name),
            weight: 50,
            diet: String::from("carnivorous"),
        }
    }

    async fn mk_app(initial: &[Dino]) -> Server<State> {
        let mut store = HashMap::new();
        for d in initial {
            store.insert(d.name.clone(), d.clone());
        }
        let state = Arc::new(RwLock::new(store));
        server(state).await
    }

    async fn call_impl<
        S: Sync + Send + Clone + 'static,
        A: Serialize,
        B: DeserializeOwned + Clone,
    >(
        app: &Server<S>,
        method: Method,
        path: &str,
        body: Option<A>,
    ) -> tide::Result<Resp<B>> {
        let url = Url::parse(format!("http://example.com/{}", path).as_str())?;
        let mut req = Request::new(method, url);
        match body {
            None => (),
            Some(b) => req.set_body(Body::from_json(&b)?),
        };
        let mut res: Response = app.respond::<Request, Response>(req).await?;
        let parsed = match res.body_json::<B>().await {
            Err(e) => Err(e),
            Ok(x) => Ok(x.clone()),
        };
        Ok(Resp {
            raw: res,
            parsed: parsed,
        })
    }

    // TODO: partial application / builder to  write
    //   call(app, method, path)
    //     1:
    //     .no_body()
    //     .body(req)
    //     2.
    //     .parsing::<Resp>(): Result<(Response, Result<Resp>)>
    //     .raw(): Result<Response>
    //
    async fn call<S: Sync + Send + Clone + 'static, B: DeserializeOwned + Clone>(
        app: &Server<S>,
        method: Method,
        path: &str,
    ) -> tide::Result<Resp<B>> {
        call_impl(app, method, path, None::<()>).await
    }

    async fn call_no_deser<S: Sync + Send + Clone + 'static>(
        app: &Server<S>,
        method: Method,
        path: &str,
    ) -> tide::Result<Response> {
        Ok(call_impl::<S, (), ()>(app, method, path, None).await?.raw)
    }

    async fn call_with_body<
        S: Sync + Send + Clone + 'static,
        A: Serialize,
        B: DeserializeOwned + Clone,
    >(
        app: &Server<S>,
        method: Method,
        path: &str,
        body: A,
    ) -> tide::Result<Resp<B>> {
        call_impl(app, method, path, Some(body)).await
    }

    async fn call_with_body_no_deser<S: Sync + Send + Clone + 'static, A: Serialize>(
        app: &Server<S>,
        method: Method,
        path: &str,
        body: A,
    ) -> tide::Result<Response> {
        Ok(call_impl::<S, A, ()>(app, method, path, Some(body))
            .await?
            .raw)
    }

    #[async_std::test]
    async fn create_dino() -> tide::Result<()> {
        let app = &mk_app(&[]).await;

        let dino = mk_dino("a");

        let res = call_with_body_no_deser(app, Method::Post, "dinos", dino.clone()).await?;
        assert_eq!(201, res.status());

        let d: Dino = call(app, Method::Get, "dinos/a").await?.parsed?;
        assert_eq!(d, dino);
        Ok(())
    }

    #[async_std::test]
    async fn read_dino() -> tide::Result<()> {
        let dino = mk_dino("a");

        let app = &mk_app(&[dino.clone()]).await;

        let d: Dino = call(app, Method::Get, "dinos/a").await?.parsed?;
        assert_eq!(d, dino);

        let resp: Response = call_no_deser(app, Method::Get, "dinos/unknown").await?;
        assert_eq!(resp.status(), 404);

        Ok(())
    }

    #[async_std::test]
    async fn update_dino() -> tide::Result<()> {
        let mut dino = mk_dino("a");

        let app = &mk_app(&[dino.clone()]).await;

        let d: Dino = call(app, Method::Get, "dinos/a").await?.parsed?;
        assert_eq!(d, dino);

        dino.weight += 100;

        let d: Dino = call_with_body(app, Method::Put, "dinos/a", dino.clone())
            .await?
            .parsed?;
        assert_eq!(d, dino);

        let d: Dino = call(app, Method::Get, "dinos/a").await?.parsed?;
        assert_eq!(d, dino);

        Ok(())
    }

    #[async_std::test]
    async fn delete_dino() -> tide::Result<()> {
        let dino = mk_dino("a");

        let app = &mk_app(&[dino.clone()]).await;

        let d: Dino = call(app, Method::Get, "dinos/a").await?.parsed?;
        assert_eq!(d, dino);

        let resp = call_no_deser(app, Method::Delete, "dinos/a").await?;
        assert_eq!(resp.status(), 200);

        let resp = call_no_deser(app, Method::Get, "dinos/a").await?;
        assert_eq!(resp.status(), 404);

        let resp = call_no_deser(app, Method::Delete, "dinos/a").await?;
        assert_eq!(resp.status(), 404);

        Ok(())
    }

    #[async_std::test]
    async fn list_dinos() -> tide::Result<()> {
        let mut dinos: Vec<Dino> = vec![mk_dino("a"), mk_dino("b"), mk_dino("c")];
        dinos.sort();

        let app = &mk_app(dinos.as_slice()).await;

        let mut v: Vec<Dino> = call(app, Method::Get, "dinos").await?.parsed?;
        v.sort();

        assert_eq!(v, dinos);

        Ok(())
    }
}
